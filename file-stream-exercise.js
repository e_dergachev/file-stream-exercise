const fs = require('fs');
const stream = fs.createReadStream('1.txt', {highWaterMark: 1});

let char = '';
stream.on('data', data => char = char + (Number(data) + 1));
stream.on('end', () => fs.createWriteStream('1.txt').write(char));
